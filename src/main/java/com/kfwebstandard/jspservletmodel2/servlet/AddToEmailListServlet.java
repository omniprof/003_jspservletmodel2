package com.kfwebstandard.jspservletmodel2.servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kfwebstandard.jspservletmodel2.model.User;
import com.kfwebstandard.jspservletmodel2.persistence.UserIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the servlet that receives a request from the join_email.jsp
 *
 * @author Ken Fogel
 */
@WebServlet(name = "AddToEmailList", urlPatterns = {"/AddToEmailList"})
public class AddToEmailListServlet extends HttpServlet {

    // All good programmers use a Logger
    private final static Logger LOG = LoggerFactory.getLogger(AddToEmailListServlet.class);

    /**
     * Handles the HTTP <code>GET</code> method. GET is used so that you can see
     * the query string in the browser's address bar
     *
     * @param request server produced object from the user
     * @param response server produced object for replying to the user
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // The JSP page to call when the servlet ius done
        String url = "/display_email.jsp";
        
        // get parameters from the request
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");

        // store data in User object
        User user = new User(firstName, lastName, email);

        // Read the context param from web.xml
        ServletContext context = getServletContext();
        String fileName = context.getInitParameter("EmailFile");

        // write the User object to a file
        UserIO userIO = new UserIO();
        userIO.addRecord(user, fileName);

        // Store the user object in the request object
        request.setAttribute("user", user);
        // forward request and response to JSP page
        RequestDispatcher dispatcher
                = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }
}
