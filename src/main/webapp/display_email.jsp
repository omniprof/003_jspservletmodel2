<%-- 
    Document   : display_email.jsp
    Created on : 2014-01-15
    Modified on: 2020-01-29
    Author     : Ken

    As a JSP file you use expression language to access the data placed in the 
    bean by the servlet
--%>

<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.time.LocalDateTime"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>003 JSP/Servlet Model 2 Output</title>
        <link rel="stylesheet" href="styles/main.css" type="text/css"/>
    </head>

    <body>
        <h1>003 - Thanks for joining our email list</h1>

        <p>Here is the information that you entered:</p>
        <label>Email:</label>
        <span>${user.emailAddress}</span><br>
        <label>First Name:</label>
        <span>${user.firstName}</span><br>
        <label>Last Name:</label>
        <span>${user.lastName}</span><br>

        <p>To enter another email address, click on the Return button shown below.</p>

        <form action="join_email.jsp" method="post">
            <input type="submit" value="Return">
        </form>
        <p>This email address was added to our list on <%= LocalDateTime.now().format(DateTimeFormatter.ISO_DATE)%></p>
    </body>
</html>