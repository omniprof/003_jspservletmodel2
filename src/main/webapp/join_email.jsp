<%-- 
    Document   : join_email.jsp
    Created on : 2014-01-15
    Modified on: 2020-01-29
    Author     : Ken Fogel

    As a JSP file you use expression language in input tags so that user input 
    is entered into the bean
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>003 JSP/Servlet Model 2 Input</title>
        <link rel="stylesheet" href="styles/main.css" type="text/css"/>
    </head>
    <body>
        <h1>003 - Join our email list</h1>
        <p>To join our email list, enter your name and
            email address below.</p>

        <form action="AddToEmailList" method="get">
            <label class="pad_top">Email:</label>
            <input type="email" name="email" value="${user.emailAddress}" required><br>
            <label class="pad_top">First Name:</label>
            <input type="text" name="firstName" value="${user.firstName}" required><br>
            <label class="pad_top">Last Name:</label>
            <input type="text" name="lastName" value="${user.lastName}" required><br>
            <label>&nbsp;</label>
            <input type="submit" value="Join Now" class="margin_left">
        </form>
    </body>
</html>